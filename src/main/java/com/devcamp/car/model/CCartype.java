package com.devcamp.car.model;

import jakarta.persistence.*;

@Entity
@Table(name = "cartype")
public class CCartype {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long cartype_id;
	
	@Column(name = "typecode", unique = true)
	private String typecode;
	
	@Column(name = "typename")
	private String typename;
	
	@ManyToOne
    @JoinColumn(name="car_id")
    private CCar car;

	public CCartype() {
	}

	public CCartype(String typecode, String typename) {
		this.typecode = typecode;
		this.typename = typename;
	}

	public long getCartype_id() {
		return cartype_id;
	}

	public void setCartype_id(long cartype_id) {
		this.cartype_id = cartype_id;
	}

	public String getTypecode() {
		return typecode;
	}

	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public CCar getCar() {
		return car;
	}

	public void setCar(CCar car) {
		this.car = car;
	}


}
