package com.devcamp.car.model;

import java.util.Set;

import jakarta.persistence.*;


import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long car_id;
	
	@Column(name = "carcode", unique = true)
	private String carCode;
	
	@Column(name = "carname")
	private String carname;
	
	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CCartype> cartypes;
	
	public CCar() {
	}

	public CCar(String carCode, String carname) {
		this.carCode = carCode;
		this.carname = carname;
	}

	public long getCar_id() {
		return car_id;
	}

	public void setCar_id(long car_id) {
		this.car_id = car_id;
	}

	public String getCarcode() {
		return carCode;
	}

	public void setCarcode(String carCode) {
		this.carCode = carCode;
	}

	public String getCarname() {
		return carname;
	}

	public void setCarname(String carname) {
		this.carname = carname;
	}

	public Set<CCartype> getCartypes() {
		return cartypes;
	}

	public void setCartypes(Set<CCartype> cartypes) {
		this.cartypes = cartypes;
	}


}
