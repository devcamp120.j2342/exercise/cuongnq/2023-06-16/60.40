package com.devcamp.car.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.car.model.CCar;
import com.devcamp.car.model.CCartype;
import com.devcamp.car.repository.ICarRepository;
import com.devcamp.car.repository.ICartypeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CarController {
	@Autowired
    ICarRepository pCarRepository;
	
	@Autowired
    ICartypeRepository pRegionRepository;
	
	@GetMapping("/devcamp-cars")
	public ResponseEntity<List<CCar>> getAllVouchers() {
        try {
            List<CCar> pCCars = new ArrayList<CCar>();

            pCarRepository.findAll().forEach(pCCars::add);

            return new ResponseEntity<>(pCCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GetMapping("/devcamp-cartypes")
	public ResponseEntity<Set<CCartype>> getRegionsByCountryCode(@RequestParam String carCode) {
        try {
            CCar vCar = pCarRepository.findBycarCode(carCode);
            
            if(vCar != null) {
            	return new ResponseEntity<>(vCar.getCartypes(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
