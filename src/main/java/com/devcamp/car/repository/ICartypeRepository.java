package com.devcamp.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.car.model.CCartype;

public interface ICartypeRepository extends JpaRepository<CCartype, Long> {

}
